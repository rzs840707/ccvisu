Release CCVisu 3.5, 2012-12-24.

- Redesign of the graphical user interface.
- JAVA SOURCECODE fact extractor integrated.
  Produces typical relation that can be found in Java source code.
- Clustering (partitioning) algorithm integrated. 
- Several bugs fixed. 


Release CCVisu 3.0, 2010-02-04.

- DOXYGEN fact extractor integrated.
  Produces relations BASEDON, COMPOUND, CONTAINEDIN, LOCATEDAT, MEMBER, REFERSTO
  so far.
- LAY files start each line with LAY now in order to create proper RSF files.
- More compatible SVG output (thanks to Luka Frelih for the patch).
  Now the interactive features of the SVG files are working
  with Firefox as well, not only with Adobe SVG Viewer.
- Fixed RSF compatibility issue of the LAY files.
