/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.util;

import java.util.concurrent.TimeUnit;

/**
 * Stopwatch-class for time measurement.
 */
public class Stopwatch {
  private long startTime = -1;
  private long stopTime = -1;
  private boolean isTicking = false;

  /**
   * Start the measurement of time.
   * @return this
   */
  public Stopwatch start() {
    startTime = System.currentTimeMillis();
    isTicking = true;
    return this;
  }

  /**
   * Stop the measurement.
   * @return this
   */
  public Stopwatch stop() {
    stopTime = System.currentTimeMillis();
    isTicking = false;
    return this;
  }

  /**
   * Get the time elapsed between the call of "start" and "stop".
   * @return elapsed time in milliseconds
   */
  public long getElapsedTime() {
     if (startTime == -1) {
       return 0;
     } else if (isTicking) {
       return System.currentTimeMillis() - startTime;
     } else {
       return stopTime - startTime;
     }
  }

  public Stopwatch reset() {
    startTime = -1;
    stopTime = -1;
    isTicking = false;
    return this;
  }

  @Override
  public String toString() {
    long msecs = this.getElapsedTime();

    return String.format("%d min, %d sec, %d msec",
        TimeUnit.MILLISECONDS.toMinutes(msecs),
        TimeUnit.MILLISECONDS.toSeconds(msecs)
          - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(msecs)),
        TimeUnit.MILLISECONDS.toMillis(msecs)
          - TimeUnit.MINUTES.toMillis(TimeUnit.MILLISECONDS.toMinutes(msecs))
          - TimeUnit.SECONDS.toMillis(TimeUnit.MILLISECONDS.toSeconds(msecs))
    );
  }

  public static Stopwatch createAndStart() {
    Stopwatch stopwatch = new Stopwatch();
    return stopwatch.start();
  }
}
