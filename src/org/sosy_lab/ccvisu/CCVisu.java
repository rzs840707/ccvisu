/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu;

import java.io.IOException;

import org.sosy_lab.ccvisu.Options.OptionsEnum;
import org.sosy_lab.ccvisu.Options.Verbosity;
import org.sosy_lab.ccvisu.graph.GraphData;
import org.sosy_lab.ccvisu.writers.Writer;
import org.sosy_lab.ccvisu.writers.WriterGui;

/**
 * Main class of the CCVisu package.
 * Contains the main program and some auxiliary methods.
 */
public class CCVisu {

  private final Options options;

  /**
   * Constructor.
   */
  public CCVisu(Options options) {
    this.options = options;
  }

  /**
   * Run the application with the graphical user interface (Swing).
   * @param controller
   */
  private void runInGuiMode(final CCVisuController controller) {
    Writer gui = new WriterGui(controller, options);
    gui.write();
  }

  /**
   * Run the application in the command line mode.
   * @throws InterruptedException
   */
  private void runInCmdLineMode(CCVisuController ccvCtrl)
      throws IOException, InterruptedException {

    if (options.verbosity.isAtLeast(Verbosity.VERBOSE)) {
      options.infoCollector.addListener(new InformationCollector.StderrMessagePrinter());
    }

    ccvCtrl.process(options);
  }

  /**
   * Main program. Performs the following steps.
   * 1) Parses and handles the command line options.
   * 2) Processes the actual task.
   * @param args  Command line arguments.
   */
  public static void main(String[] args) {
    // Created required instances.
    Options options = new Options(new GraphData());
    options.parseCmdLine(args);

    // Help: Print help text and terminate CCVisu.
    if (options.getOption(OptionsEnum.help).getBool()) {
      System.out.println(options.helpMessage());
      return;
    }

    // Version: Print version and terminate CCVisu.
    if (options.getOption(OptionsEnum.version).getBool()) {
      System.out.println(options.versionMessage());
      return;
    }

    // Check if code is compiled with assertions and terminate CCVisu.
    if (options.getOption(OptionsEnum.assertCheck).getBool()) {
      boolean assertsEnabled = false;
      assert (assertsEnabled = true); // Intentional side effect!!!
      if (assertsEnabled) {
        System.out.println("Assertions are enabled.");
      } else {
        System.out.println("Assertions are disabled.");
      }

      return;
    }

    options.infoCollector = new InformationCollector();
    CCVisu ccVisu = new CCVisu(options);

    // Run CCVisu.
    try {
      CCVisuController ccvCtrl = new CCVisuController();

      // Run CCVisu depending on the operation mode
      if (options.getOption(OptionsEnum.guiMode).getBool()) {
        ccVisu.runInGuiMode(ccvCtrl);
      } else {
        ccVisu.runInCmdLineMode(ccvCtrl);
      }
    } catch (Exception e) {
      System.err.println(e.getMessage());
      System.exit(1);
    }
  }
}
