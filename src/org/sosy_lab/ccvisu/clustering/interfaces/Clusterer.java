/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.clustering.interfaces;

import java.util.List;

import org.sosy_lab.ccvisu.graph.GraphData;
import org.sosy_lab.ccvisu.graph.Group;

/**
 * Interface that describes a class that is able to cluster
 * a graph into partitions.
 */
public interface Clusterer {
  /**
   * Create partitions of a given graph layout.
   *
   * @param graphData  The graph that should be clustered.
   * @return           List of created partitions.
   *
   * @throws InterruptedException
   */
  public List<Group> createClustersOfLayout(GraphData graphData)
  throws InterruptedException;

  /**
   * Run the clustering (as defined in createClustersOfLayout)
   * and write the partitions to the groups-property of the graph.
   *
   * @param graphData  The graph that should be clustered.
   *
   * @throws InterruptedException
   */
  public void runClusteringAndUpdateGroups(GraphData graphData)
  throws InterruptedException;
}
