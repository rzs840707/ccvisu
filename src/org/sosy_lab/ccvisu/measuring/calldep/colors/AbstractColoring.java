/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.measuring.calldep.colors;

import java.awt.Color;

/**
 * Abstract class provides basis to calculate a linear color gradient.
 */
public abstract class AbstractColoring implements VertexColoring {

  private double min;
  private double max;

  public AbstractColoring(double min, double max) {
    this.max = max;
    this.min = min;
  }

  @Override
  abstract public Color getColor(double degree);

  /**
   * Returns the value in relation to the minimum and maximum in the set.
   *
   * @return value in [0,1]
   */
  protected double getRelativeValue(double degree) {
    return ((degree - min)/max);
  }

  /**
   *
   * @param relDegree must be in [0,1]
   * @return a Color between green and red
   */
  protected Color getColorGreenToRed(float relDegree) {
    assert relDegree >= 0.0f && relDegree <= 1.0f;

    float red = 0;
    float green = 0;
    float blue = 0;

    if (relDegree < 0.5) {
      // yellow to green
      green = 1;
      red = 2 * relDegree;
    } else {
      // red to yellow
      red = 1;
      green = (1 - relDegree) * 2;
    }

    return new Color(red, green, blue);
  }
}
