/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;

import org.sosy_lab.ccvisu.Options;
import org.sosy_lab.ccvisu.ui.controlpanel.ToolPanelClusterer;
import org.sosy_lab.ccvisu.ui.controlpanel.ToolPanelConsole;
import org.sosy_lab.ccvisu.ui.controlpanel.ToolPanelFilters;
import org.sosy_lab.ccvisu.ui.controlpanel.ToolPanelGroups;
import org.sosy_lab.ccvisu.ui.controlpanel.ToolPanelLayouter;
import org.sosy_lab.ccvisu.ui.controlpanel.ToolPanelMeasurer;
import org.sosy_lab.ccvisu.ui.controlpanel.ToolPanelNodeManager;
import org.sosy_lab.ccvisu.writers.WriterDataLayoutDISP;
import org.sosy_lab.util.interfaces.WorkerManager;

public class FrameTools extends JToolBar {

  private static final long    serialVersionUID = -3397665367586164045L;

  private ToolPanelLayouter    minimizerPanel;
  private ToolPanelClusterer   clusteringPanel;
  private ToolPanelMeasurer    dependencyPanel;
  private ToolPanelGroups      groupsPanel;
  private ToolPanelNodeManager nodeManagerPanel;
  private ToolPanelConsole     consolePanel;
  private ToolPanelFilters     filterPanel;

  public FrameTools(Options options, WriterDataLayoutDISP writer,
      WorkerManager workerManager) {

    assert workerManager != null;

    setName("CCVisu | Tools");

    minimizerPanel = new ToolPanelLayouter(options, workerManager);
    clusteringPanel = new ToolPanelClusterer(options, workerManager);
    groupsPanel = new ToolPanelGroups(writer, options.graph);
    dependencyPanel = new ToolPanelMeasurer(options, workerManager);
    nodeManagerPanel = new ToolPanelNodeManager(writer, options.graph);
    consolePanel = new ToolPanelConsole(options.infoCollector);
    filterPanel = new ToolPanelFilters(options);

    initComponents();
    loadOptions();
  }

  private void initComponents() {
    this.setLayout(new BorderLayout());

    // Grouper
    JPanel clusteringTab = new JPanel();
    clusteringTab.setLayout(new BorderLayout());
    clusteringTab.add(groupsPanel, BorderLayout.CENTER);
    clusteringTab.add(clusteringPanel, BorderLayout.SOUTH);

    // Layouter
    JPanel minimizerTab = new JPanel();
    minimizerTab.setLayout(new FlowLayout());
    minimizerTab.add(minimizerPanel);

    // Measurer
    JPanel dependencyTab = new JPanel();
    dependencyTab.setLayout(new FlowLayout());
    dependencyTab.add(dependencyPanel);

    // Node Manager
    JPanel nodeManagerTab = new JPanel();
    nodeManagerTab.setLayout(new BorderLayout());
    nodeManagerTab.setBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7));
    nodeManagerTab.add(nodeManagerPanel, BorderLayout.CENTER);

    // Info Console
    JPanel consoleTab = new JPanel();
    consoleTab.setLayout(new BorderLayout());
    consoleTab.setBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7));
    consoleTab.add(consolePanel, BorderLayout.CENTER);

    // Filter console
    JPanel filterTab = new JPanel();
    filterTab.setLayout(new FlowLayout());
    filterTab.add(filterPanel);

    // All tools
    JTabbedPane tabbedPane = new JTabbedPane();
    tabbedPane.addTab("Node Manager", nodeManagerTab);
    tabbedPane.addTab("Console", consoleTab);
    tabbedPane.addTab("Layouter", minimizerTab);
    tabbedPane.addTab("Grouper", clusteringTab);
    tabbedPane.addTab("Measurer", dependencyTab);
    tabbedPane.addTab("Filters", filterTab);

    this.add(tabbedPane, BorderLayout.CENTER);
  }

  public void applyOptions() {
    minimizerPanel.applyOptions();
    clusteringPanel.applyOptions();
    dependencyPanel.applyOptions();
  }

  public void loadOptions() {
    minimizerPanel.loadOptions();
    clusteringPanel.loadOptions();
    dependencyPanel.loadOptions();
  }
}
