/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.ui.helper;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.sosy_lab.ccvisu.graph.GraphVertex.Shape;

public class ShapeCellRenderer extends JLabel implements ListCellRenderer<Shape>{
  private static final long serialVersionUID = 7439957927178451881L;

  private Color color;

  @Override
  public Component getListCellRendererComponent(JList<? extends Shape> list, Shape value,
      int index, boolean isSelected, boolean cellHasFocus) {

    Shape shape = (Shape) value;
    setText(value.toString());
    setIcon(new ShapeIcon(shape));

    return this;
  }

  public void setColor(Color pColor) {
    this.color = pColor;
  }

  private class ShapeIcon implements Icon {
    private final int height = 10;
    private final int width  = 10;

    private Shape    shape;

    public ShapeIcon(Shape shape) {
      this.shape = shape;
    }

    @Override
    public int getIconHeight() {
      return height;
    }

    @Override
    public int getIconWidth() {
      return width;
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int x, int y) {

      // use selected color of colorComboBox to display shapes
      graphics.setColor(ShapeCellRenderer.this.color);

      if ((shape == Shape.BOX) || (shape == Shape.FIXED_SIZE_BOX)) {
        // BOX
        graphics.fillRect(x, y, width, width);

        graphics.setColor(Color.GRAY);
        graphics.drawRect(x, y, width, width);

      } else if (shape == Shape.DISC) {
        // DISC
        graphics.fillOval(x, y, width, width);

        graphics.setColor(Color.GRAY);
        graphics.drawOval(x, y, width, width);

      } else if (shape == Shape.RBOX) {
        // RBox
        float d = width;
        graphics.fillRoundRect(x, y, width, width,
            (int) (3 * d / 4), (int) (3 * d / 4));

        graphics.setColor(Color.GRAY);
        graphics.drawRoundRect(x, y, width, width,
            (int) (3 * d / 4), (int) (3 * d / 4));

      } else if (shape == Shape.FILLER_RECT) {
        // FILLER_RECT
        graphics.fillRect(x, y, height, width);

        graphics.setColor(Color.GRAY);
        graphics.drawRect(x, y, height, width);

      } else {
        // UNKNOWN
        assert (false);
      }
    }
  }

}
