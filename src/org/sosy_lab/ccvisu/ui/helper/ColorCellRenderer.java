/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.ui.helper;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.sosy_lab.util.Colors;

@SuppressWarnings("serial")
public class ColorCellRenderer extends JLabel implements ListCellRenderer<Colors> {

  public ColorCellRenderer() {
    setOpaque(true);
  }

  @Override
  public Component getListCellRendererComponent(JList<? extends Colors> list, Colors value,
      int index, boolean isSelected, boolean cellHasFocus) {

    Colors colors = (Colors) value;
    setText(colors.toString());
    setIcon(new ColorsIcon(colors));

    return this;
  }

  private static class ColorsIcon implements Icon {
    private final int height = 12;
    private final int width  = 12;

    private Colors    colors;

    public ColorsIcon(Colors colors) {
      this.colors = colors;
    }

    @Override
    public int getIconHeight() {
      return height;
    }

    @Override
    public int getIconWidth() {
      return width;
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int x, int y) {
      // draw rectangle
      graphics.setColor(Color.black);
      graphics.drawRect(x, y, width - 1, height - 1);

      // fill rectangle with color this.color
      graphics.setColor(colors.get());
      graphics.fillRect(x, y, width - 1, height - 1);
    }
  }

}