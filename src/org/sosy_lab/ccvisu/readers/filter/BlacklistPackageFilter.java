/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.readers.filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.sosy_lab.ccvisu.graph.Relation;
import org.sosy_lab.ccvisu.graph.Tuple;

/**
 * Filter that removes all relations where either the first or second element
 * starts with a given String.
 *
 * In the context of software graphs this represents the package name.
 */
public class BlacklistPackageFilter implements Filter {

  private Collection<String> blockedPackages;

  public BlacklistPackageFilter(Collection<String> blockedPackages) {
    assert blockedPackages != null;

    this.blockedPackages = new ArrayList<String>(blockedPackages);
  }

  @Override
  public Relation apply(Relation rel) {
    assert rel != null;

    Relation newRel = new Relation();

    for (Tuple tuple : rel) {
      addTupleUnlessBlacklisted(newRel, tuple);
    }

    return newRel;
  }

  private void addTupleUnlessBlacklisted(Relation rel, Tuple tuple) {
    for (String blockedPackage : blockedPackages) {
      List<String> elements = tuple.getTupleElements();
      assert elements.size() >= 2;

      if (elements.get(0).startsWith(blockedPackage)
          || elements.get(1).startsWith(blockedPackage)) {
        return;
      }
    }

    rel.addTuple(tuple);
  }
}
