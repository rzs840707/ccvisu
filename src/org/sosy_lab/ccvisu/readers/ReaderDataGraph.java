/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.readers;

import java.io.BufferedReader;

import org.sosy_lab.ccvisu.CCVisuController;
import org.sosy_lab.ccvisu.Options.Verbosity;
import org.sosy_lab.ccvisu.graph.GraphData;
import org.sosy_lab.ccvisu.graph.Relation;

/**
 * Reader for input graphs.
 * Different concrete graph readers return what they read in String format
 * (list of edges of type <code>GraphEdgeString</code>)
 * when <code>readEdges()</code> is called.
 * One single transformation method (<code>readGraph()</code> of this class)
 * transforms the string representation into the final format
 * (<code>GraphData</code> object with edges of type <code>GraphEdgeInt</code>).
 */
public abstract class ReaderDataGraph extends ReaderData {

  /** End of line.*/
  protected final static String endl = CCVisuController.endl;

  /**
   * Constructor.
   * @param reader  Stream reader object.
   */
  public ReaderDataGraph(BufferedReader reader, Verbosity verbosity) {
    super(reader, verbosity);
  }

  /**
   * Reads the edges of a graph from stream reader <code>in</code>,
   * and stores them in a list (of <code>GraphEdgeString</code> elements).
   * @return List of string edges.
   */
  abstract public Relation readTuples();

  /**
   * Reads the graph data from stream reader <code>in</code>.
   * @param graph  <code>GraphData</code> object to store the graph data in.
   */
  @Override
  public void read(GraphData graph) {
    graph.applyRelation(readTuples(), verbosity);
  }
}
