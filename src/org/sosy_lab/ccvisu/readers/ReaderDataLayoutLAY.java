/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.readers;

import java.awt.Color;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.sosy_lab.ccvisu.Options.Verbosity;
import org.sosy_lab.ccvisu.graph.GraphData;
import org.sosy_lab.ccvisu.graph.GraphVertex;
import org.sosy_lab.ccvisu.graph.NameVisibility;
import org.sosy_lab.ccvisu.graph.Position;
import org.sosy_lab.ccvisu.graph.Relation;
import org.sosy_lab.ccvisu.graph.Tuple;

/**
 * Reader for layouts in text format.
 */
public class ReaderDataLayoutLAY extends ReaderData {

  /**
   * Constructor.
   * @param reader  Stream reader object.
   */
  public ReaderDataLayoutLAY(BufferedReader reader, Verbosity verbosity) {
    super(reader, verbosity);
  }

  /**
   * Reads the layout data from stream reader <code>in</code>, in text format LAY.
   * @param graph   <code>GraphData</code> object to store the layout data in.
   */
  @Override
  public void read(GraphData graph) {
    List<GraphVertex> vertices = new ArrayList<GraphVertex>();
    ReaderDataGraphRSF rsfReader = new ReaderDataGraphRSF(reader, verbosity);
    Relation layout = rsfReader.readTuples();

    for (Tuple tuple : layout) {

      // provides backwards compatibility
      List<String> vertex = new ArrayList<String>();
      vertex.add(tuple.getRelationName());
      vertex.addAll(tuple.getTupleElements());

      if (vertex.size() < 6) {
        System.err.println("RSF reading problem. Following format is expected:");
        System.err.println("  LAY <x-pos_float> <y-pos_float> <z-pos_float> "
            + "<edge-degree_int> <vertex-name_string> [<rgb-color_int>] "
            + "[<showName_bool>]");
      }

      Iterator<String> it = vertex.iterator();

      if (tuple.getRelationName().equals("LAY")) {
        // To handle old LAY files without the 'LAY' as relation name.
        // 'vertex.get(0)' contains actually the x coordinate.
        it.next();
      }

      float xValue = Float.parseFloat(it.next());
      float yValue = Float.parseFloat(it.next());
      float zValue = Float.parseFloat(it.next());
      float degree = Float.parseFloat(it.next());

      String name = it.next();
      Position newPos = new Position(xValue, yValue, zValue);
      GraphVertex newVertex = new GraphVertex(name, newPos, degree);

      // optional: color
      if (it.hasNext()) {
        newVertex.setColor(new Color(Integer.parseInt(it.next())));
      }

      // optional: show label
      if (it.hasNext()) {
        boolean showVertexLabel = Boolean.valueOf(it.next()).booleanValue();
        newVertex.setShowName(NameVisibility.Priority.STARTUP, showVertexLabel);
      }

      vertices.add(newVertex);
    }
    graph.setVertices(vertices);
  }
}
