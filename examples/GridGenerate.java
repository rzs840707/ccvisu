/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu. 
 * 
 * Copyright (C) 2005-2010  Dirk Beyer
 * 
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 * 
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */


public class GridGenerate {

  final static public int size = 40;

  public static void main(String[] args) {
    for (int j = 0; j < GenerateGrid.size-1; ++j) {
      System.out.println("R " + 0 + '_' + j + ' ' + 0 + '_' + (j+1));
    }
    for (int i = 0; i < GenerateGrid.size; ++i) {
      // j is the column, i is the row number.
      for (int j = 0; j < GenerateGrid.size; ++j) {
        if (j < GenerateGrid.size-1) {
	  System.out.println("R " + (i+1) + '_' + j + ' ' + (i+1) + '_' + (j+1));
	}
	System.out.println("R " + i + '_' + j + ' ' + (i+1) + '_' + j);
      }
    }
  }
}
